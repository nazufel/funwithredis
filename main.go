package main

import (
	"fmt"

	"github.com/go-redis/redis"
)

func main() {
	// dial the redis server
	redisdb := redis.NewClient(&redis.Options{
		Addr:     ":6379",
		Password: "",
		DB:       0,
	})

	// stub data
	key := "favoriteMovie"
	value := "Casablanca"

	// set values in redis
	err := redisdb.Set(key, value, 0).Err()
	// check for the error
	if err != nil {
		panic(err)
	}

	// get the key value and check the error
	val, err := redisdb.Get(key).Result()
	if err != nil {
		panic(err)
	}

	// print the value
	fmt.Printf("Favorite movie is: %v\n", val)

	// get value of a missing key
	val2, err := redisdb.Get("missing_key").Result()
	// if there's nothing there, print an error message
	if err == redis.Nil {
		fmt.Println("missing_key does not exist")
		// if a system error, print
	} else if err != nil {
		panic(err)
		// print the key and value
	} else {
		fmt.Println("missing_key", val2)
	}
}
